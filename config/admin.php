<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Default admin user
    |--------------------------------------------------------------------------
    |
    | Default user will be created at project installation/deployment
    |
    */
    'admin_name' => env('ADMIN_NAME', ''),
    'admin_email' => env('ADMIN_EMAIL', ''),
    'admin_password' =>env('ADMIN_PASSWORD', ''),

    'smart-contract-address' => '0x719035ac096b12aad44578a2db8a352ad874892d',
    'token-symbol' => 'VNCT',

    'support_mail' => 'info@vincoin.cash',
    'white-paper_url' => 'storage/VNC - White Paper.pdf',

    'project_video_links' => [
        'en' => [
            'easy_way_try_crypto' => 'https://www.youtube.com/watch?v=DBfvJuj7_cQ',
            'how_to_mine' => 'https://www.youtube.com/watch?v=SrJCR_-4ncY',
            'how_to_create_wallet' => 'https://www.youtube.com/watch?v=yF0MyE-Zo74',
        ],
        'ru' => [
            'easy_way_try_crypto' => 'https://www.youtube.com/watch?v=2JGjCQmJPUE',
            'how_to_mine' => 'https://www.youtube.com/watch?v=SrJCR_-4ncY',
            'how_to_create_wallet' => 'https://www.youtube.com/watch?v=yF0MyE-Zo74',
        ],
        'vn' => [
            'easy_way_try_crypto' => 'https://www.youtube.com/watch?v=duQaz_lcNd8',
            'how_to_mine' => 'https://www.youtube.com/watch?v=SrJCR_-4ncY',
            'how_to_create_wallet' => 'https://www.youtube.com/watch?v=yF0MyE-Zo74',
        ],
    ]
];
