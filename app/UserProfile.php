<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_profiles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'telegram_nickname'
    ];

    protected $casts = [
        'is_telegram_joined' => 'boolean',
        'is_facebook_joined' => 'boolean',
        'is_twitter_joined' => 'boolean',
    ];


    /**
     * Get the user that owns the profile.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
