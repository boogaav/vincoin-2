<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cookie;

class CheckReferrer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->cookie("ref_id")) && $request->filled("ref_id")) {
            Cookie::queue('ref_id', $request->get("ref_id"));
        }
        return $next($request);
    }
}
