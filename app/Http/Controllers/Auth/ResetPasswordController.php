<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\Localization\LocalizationHelper;
use Illuminate\Foundation\Auth\ResetsPasswords;
//use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/account';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm($req_locale = null, $token = null)
    {
//        dd($request->email);
//        dd(request()->route('token'));
        $url_locale = LocalizationHelper::getRouteLocale();
        if (is_null($url_locale)) {
            return view('auth.passwords.reset')->with(
                ['token' => $req_locale]
            );
        } else {
            return view('auth.passwords.reset')->with(
                ['token' => $token]
            );
        }
    }
}
