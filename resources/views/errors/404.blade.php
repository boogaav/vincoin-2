<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>{{ isset($title) ? $title : __("Vincoin Cash | The Easiest Way to Enter into Crypto!") }}</title>
  <link rel="shortcut icon" type="image/png" href="/vnc_fav.png"/>
  <link rel="stylesheet" href="{{ asset("/css/app.css") }}">
</head>
<body>
  <div class="page-not-found">
    <div class="page-not-found__mars"></div>
    <img src="https://mars-404.templateku.co/img/404.svg" class="page-not-found__logo-404" />
    <img src="https://mars-404.templateku.co/img/meteor.svg" class="page-not-found__meteor" />
    <p class="page-not-found__title">Oh no!!</p>
    <p class="page-not-found__subtitle">
      You’re either misspelling the URL <br /> or requesting a page that's no longer here.
    </p>
    <div class="text-center">
      <a class="page-not-found__btn-back" href="{{ url()->previous() }}">Back to previous page</a>
    </div>
    <img src="https://mars-404.templateku.co/img/astronaut.svg" class="page-not-found__astronaut" />
    <img src="https://mars-404.templateku.co/img/spaceship.svg" class="page-not-found__spaceship" />
  </div>
</body>
</html>


