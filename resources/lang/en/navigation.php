<?php

return [
    'main' => [
        'Navigation' => 'Navigation',
        'Menu' => 'Menu',
        'Close Menu' => 'Close Menu',
        'Back' => 'Back',
        'Get started' => 'Get started',
        'Mission' => 'Mission',
        'Road map' => 'Road map',
        'About us' => 'About us',
        'Services' => 'Services',
        'Buy Vincoin Cash' => 'Buy Vincoin Cash',
        'Trade Vincoin Cash' => 'Trade Vincoin Cash',
        'Mine Vincoin Cash' => 'Mine Vincoin Cash',
        'White Paper' => 'White Paper',
        'Block explorer' => 'Block explorer',
        'Faq' => 'Faq',
        'Contact Us' => 'Contact Us',
        'Sign In/Sign Up' => 'Sign In/Sign Up',
        'Sign In' => 'Sign In',
        'Account' => 'Account',
        'Log out' => 'Log out',
        'Download' => 'Download',
        'Write to support' => 'Write to support',
        'Information' => 'Information',
        'Testnet is live' => 'Testnet is live'
    ]
];
